When you check out this project, navigate to the project folder and type:

// to download the crud module dependency
:~$ play dependencies  

// to create eclipse project files
:~$ play eclipsify

// to run the project
:~$ play run

// to test the project
:~$ play test
... and in you browser go to http://localhost:9000/@tests
