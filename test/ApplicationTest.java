import org.junit.*;

import play.test.*;
import play.db.jpa.JPABase;
import play.mvc.*;
import play.mvc.Http.*;
import models.*;

public class ApplicationTest extends FunctionalTest {

	@Test
	public void testThatLoginPageWorks() {
		Response response = GET("/users/login");
		assertIsOk(response);
		assertContentType("text/html", response);
		assertCharset(play.Play.defaultWebEncoding, response);
	}

}