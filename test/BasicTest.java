import org.junit.*;

import java.util.*;

import play.test.*;
import models.*;

public class BasicTest extends UnitTest {

    @Test
    public void aVeryImportantThingToTest() {
        assertEquals(2, 1 + 1);
    }

	@Test
	public void testInserAndRetrieve() {
		Movie movie = new Movie("Superman", 2013);
		movie.create();

		Movie retrieved = Movie.findById(movie.id);
		assertEquals("Superman", retrieved.title);
		assertEquals(new Integer(2013), retrieved.year);
	}

}
