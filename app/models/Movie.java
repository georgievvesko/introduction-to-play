package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Blob;
import play.db.jpa.Model;
import play.db.jpa.GenericModel.JPAQuery;

@Entity
public class Movie extends Model {

    public String title;
    public Integer year;
    public Blob cover;

    public Movie(String title, Integer year, Blob cover) {
        this.title = title;
        this.year = year;
        this.cover = cover;
    }

    public Movie(String title, Integer year) {
        this.title = title;
        this.year = year;
    }

    public static List<Movie> findByTitle(String title) {
        return find("byTitleLike", "%" + title + "%").fetch();
    }
}
