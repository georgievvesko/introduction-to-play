package controllers;

import play.*;
import play.db.jpa.Blob;
import play.libs.MimeTypes;
import play.mvc.*;
import play.mvc.results.Result;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.*;

import models.*;

/**
 * 
 */
@With(Security.class)
public class Movies extends Controller {

    @Before
    static void setConnectedUser() {
        if (Security.isConnected()) {
            User user = Security.getConnectedUser();
            renderArgs.put("user", user);
        }
    }

    public static void index() {
        List<Movie> movies = Movie.findAll();
        render(movies);
    }

    public static void delete(long MovieId) {
        Movie movie = Movie.findById(MovieId);
        movie.delete();
        index();
    }

    public static void create(String title, int year, File cover)
            throws FileNotFoundException {
        Movie movie = new Movie(title, year);

        if (cover != null) {
            movie.cover = new Blob();

            movie.cover.set(new FileInputStream(cover),
                    MimeTypes.getContentType(cover.getName()));
        }
        movie.create();
        index();
    }

    public static void renderImage(long movieId) {
        Movie movie = Movie.findById(movieId);

        response.setContentTypeIfNotSet(movie.cover.type());
        InputStream cover = movie.cover.get();

        if (cover != null) {
            renderBinary(cover);
        } else {
            renderBinary(new File("public/images/defaultCover.png"));
        }
    }

    public static void find(String title) {
        List<Movie> found = Movie.findByTitle(title);
        renderJSON(found);
    }
}